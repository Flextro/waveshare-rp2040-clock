#include "veml7700.h"

void veml_init(void)
{
    uint8_t buf[4];

    buf[0] = 0; //conf
    buf[1] = 0b00001000; //gain x2
    buf[2] = 0;

    i2c_write_blocking(I2C_PORT, VEML_ADDR, buf, 3, false);
}

float veml_read(void)
{
    //float lux_per_bit = 0.0576; //gain x1 100ms integration
    float lux_per_bit = 0.0288; //gain x2 100ms integration
    float lux;

    uint8_t buf[4];


    buf[0] = 0x04; //reg addr
    i2c_write_blocking(I2C_PORT, VEML_ADDR, buf, 1, true); //write reg addr; do not issue stop confition
    i2c_read_blocking(I2C_PORT, VEML_ADDR, buf, 2, false);

    uint16_t lux_raw = (buf[1] << 8 | buf [0]);
    lux = (float)lux_raw * lux_per_bit;
    return lux;
}
